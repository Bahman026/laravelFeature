<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
class NoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // dd(User::count());
            return [
                'userId' => $this->faker->numberBetween(1, User::count()),
                'text' => $this->faker->text(),
                // 'remember_token' => Str::random(10),
            ];
        
    }
}
