<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Note extends Model
{
    use HasFactory;
    protected $table = 'note';
    protected $primaryKey = 'noteId';
    protected $fillable = ['text', 'userId',];

    public function notes(){
        return $this->belongTo(User::class,'userId');
}

}
